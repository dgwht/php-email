# SMTP邮件发送

> 安装

```bash
composer require dgwht/email
```



> 邮件发送

```php
use dgwht\Email;

// 端口非25时自动采用ssl方式
Email::setConfig([
    "host" => "smtp.exmail.qq.com",
    "port" => "25",
    "user" => "admin@dgwht.com",
    "pass" => "xxxxxxxxx",
    "email" => "admin@dgwht.com",
]);

$ret = Email::send('2393176043@qq.com','标题测试','内容测试');

if($ret === true){
    echo "发送成功";
}else{
    echo "发送失败：[{$ret}]";
}

