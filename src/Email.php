<?php
namespace dgwht;

class Email {
	private static $Config;
	
	public static function setConfig($config=[]) {
		//邮件服务器
        self::$Config['host'] = isset($config['host']) ? $config['host'] : '';
		//端口
        self::$Config['port'] = isset($config['port']) ? $config['port'] : '25';
		//用户名
        self::$Config['user'] = isset($config['user']) ? $config['user'] : '';
		//密码
        self::$Config['pass'] = isset($config['pass']) ? $config['pass'] : '';
		//发件邮箱
        self::$Config['email'] = isset($config['email']) ? $config['email'] : $config['user'];
	}
	
	public static function send( $email=null, $title='标题', $text='内容' ) {
		if($email==null){
			return "收件人不能为空";
		}
		try {
		    
            $mailObj = new \dgwht\smtp\Smtp();
            
            if(self::$Config['port'] == 25){
                $mailObj->setServer(self::$Config['host'], self::$Config['user'], self::$Config['pass']); //设置smtp服务器，普通连接方式
            }else{
                $mailObj->setServer(self::$Config['host'], self::$Config['user'], self::$Config['pass'], self::$Config['port'], true); //设置smtp服务器，到服务器的SSL连接
            }
            
            $mailObj->setFrom(self::$Config['user']); //设置发件人
            $mailObj->setReceiver($email); //设置收件人，多个收件人，调用多次
            $mailObj->setMail($title, $text); //设置邮件主题、内容
            
            if($mailObj->sendMail()){
                $ret = "OK";
            }else{
                $ret = $mailObj->error();
            }
		} catch ( \Exception $e ) {
			$ret = $e;
		}
		
		if ($ret == "OK") {
			return true;
		} else {
			return $ret;
		}
	}
	
// 	public function sendMail(){
	    
//         $mailObj = new \dgwht\smtp\Smtp();
//         // $mail->setServer("smtp@126.com", "XXXXX@126.com", "XXXXX"); //设置smtp服务器，普通连接方式
//         $mailObj->setServer("smtp.exmail.qq.com", "wht@dgwht.com", "xxxxx", 465, true); //设置smtp服务器，到服务器的SSL连接
//         $mailObj->setFrom("wht@dgwht.com"); //设置发件人
//         $mailObj->setReceiver("admin@dgwht.com"); //设置收件人，多个收件人，调用多次
//         // $mail->setCc("XXXX"); //设置抄送，多个抄送，调用多次
//         // $mail->setBcc("XXXXX"); //设置秘密抄送，多个秘密抄送，调用多次
//         // $mail->addAttachment("XXXX"); //添加附件，多个附件，调用多次
//         $mailObj->setMail("test", "<b>test</b>"); //设置邮件主题、内容
//         $mailObj->sendMail(); //发送
//         echo $mailObj->error();
// 	}
}